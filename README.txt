Hypothèses et choix retenus selon l’énoncé :

- L’api sera réalisée en Anglais au cas où son usage/développement ne se limiterait pas à la France

- Modèle :
    o   Concernant la pièce de rattachement(room) du VoletRoulant (RollerShutter): j’ai assumé que pour l’exercice cette donnée était descriptive et non-utilisée fonctionnellement (ex : ouvrir tous les stores d’une pièce). Le cas échéant la pièce aurait été créé dans une classe à part (id, étage, label, luminositéAmbiante…) pour pouvoir être exploité plus aisément dans des services annexes.
    o   Pour la création d’un VoletRoulant : je suis parti du principe que seul le libellé (label) était obligatoire (l’id étant généré automatiquement). La pièce sera null si non-spécifiée, la valeur par défaut de fermeture du volet (closing) sera de 0, et l’état (isConnected) à false.

- Services :
    o   Concernant la récupération de l’état d’un Volet roulant : plutôt que de développer un service renvoyant uniquement cette information, je propose un service de récupération de l’objet VoletRoulant afin d’éviter la création de plusieurs nano-services (par exemple si on veut récupérer plus tard la pièce, ou la valeur fermeture).
    o   Concernant le service d’ouverture / fermeture d’un Volet roulant : J’ai choisi un service de type PATCH pour une modification partiel de l’objet car c’est une commande qui risque de revenir souvent. Je ne veux pas imposer aux clients l’utilisation d’un objet complet VoletRoulant (via un PUT par ex.) et la requête sera de fait plus légère.
        - Si un écran d'update de l'objet (Libellé, pièce, fermeture...) est demandé il faudra créer un service d'update avec PUT.

- Persistance :
    o   Une map est utilisée pour stocker les données. Un jeu de test a été ajouté pour simuler des données déjà existantes.