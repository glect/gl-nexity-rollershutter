package com.nexity.dev.test.rollershutter.persistence.impl;

import com.nexity.dev.test.rollershutter.model.RollerShutter;
import com.nexity.dev.test.rollershutter.persistence.RollerShutterDao;
import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

@Component
public class RollerShutterDaoImpl implements RollerShutterDao {

    /**
     * Managing store in a ConcurrentHashMap for the data persistence threadsafe, and initialization with dummy data
     */
    private final static AtomicLong idCounter = new AtomicLong(0);
    private static ConcurrentHashMap<Long, RollerShutter> rollerShutterDatas = new ConcurrentHashMap<>();
    static {
        rollerShutterDatas.put(idCounter.incrementAndGet(), new RollerShutter(idCounter.get(), "Volet exterieur veranda", "Veranda", 100, true));
        rollerShutterDatas.put(idCounter.incrementAndGet(), new RollerShutter(idCounter.get(), "Volet evier", "Cuisine", 50, true));
        rollerShutterDatas.put(idCounter.incrementAndGet(), new RollerShutter(idCounter.get(), "Volet homecinema", "Salon", 25, false));
        rollerShutterDatas.put(idCounter.incrementAndGet(), new RollerShutter(idCounter.get(), "Volet double-porte", "Salon", 0, true));
    }


    @Override
    public RollerShutter create(RollerShutter rollerShutter) {
        long newId = idCounter.incrementAndGet();
        rollerShutter.setId(newId);
        rollerShutterDatas.put(newId, rollerShutter);
        return rollerShutter;
    }

    @Override
    public RollerShutter delete(long id) {
        return rollerShutterDatas.remove(id);
    }

    @Override
    public RollerShutter get(long id) {
        return rollerShutterDatas.get(id);
    }

    @Override
    public RollerShutter updateClosing(long id, int closing) {
        RollerShutter rsToUpdate = rollerShutterDatas.get(id);
        if(rsToUpdate != null){
            rsToUpdate.setClosing(closing);
            rsToUpdate = rollerShutterDatas.replace(id, rsToUpdate);
        }
        return rsToUpdate;
    }
}
