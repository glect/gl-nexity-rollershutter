package com.nexity.dev.test.rollershutter.model.validator;

import com.nexity.dev.test.rollershutter.model.RollerShutter;

/**
 * Validation Rules for model RollerShutter
 */
public class RollerShutterValidator {

    public static final String IS_VALID_FOR_CREATION_ERROR = "Creation declined. Label is required";
    public static final String IS_CLOSING_VALUE_ACCEPTABLE_ERROR = "Bad closing value, must be between 0 - 100.";
    /**
     * Check if the RollerShutter has a Label and is not null
     * @param rollerShutter
     * @return true or false
     */
    public static boolean isValidForCreation(RollerShutter rollerShutter){
        if(rollerShutter != null){
            if(rollerShutter.getLabel() != null && !rollerShutter.getLabel().trim().isEmpty()){
                return true;
            }
        }
        return false;
    }

    /**
     * Check if closing value is between 0 - 100
     * @param closing
     * @return true or false
     */
    public static boolean isClosingValueAcceptable(int closing){
        if(0 <= closing && closing <= 100){
            return true;
        }
        return false;
    }
}
