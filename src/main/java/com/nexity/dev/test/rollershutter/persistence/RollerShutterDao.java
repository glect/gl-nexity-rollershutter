package com.nexity.dev.test.rollershutter.persistence;

import com.nexity.dev.test.rollershutter.model.RollerShutter;

public interface RollerShutterDao {

    /**
     * Create a new RollerShutter
     * @param rollerShutter
     * @return the new RollerShutter with its new id
     */
    public RollerShutter create(RollerShutter rollerShutter);

    /**
     * Delete the RollerShutter
     * @param id
     * @return Return the deleted RollerShutter, or null if not found
     */
    public RollerShutter delete(long id);

    /**
     * Recover a RollerShutter by its id
     * @param id
     * @return The RollerShutter, or null if not found
     */
    public RollerShutter get(long id);

    /**
     * Update the closing value of the RollerShutter
     * @param id
     * @param closing
     * @return The updated RollerShutter with the closing, null if not found
     */
    public RollerShutter updateClosing(long id, int closing);

}
