package com.nexity.dev.test.rollershutter.controller;

import com.nexity.dev.test.rollershutter.model.RollerShutter;
import com.nexity.dev.test.rollershutter.model.validator.RollerShutterValidator;
import com.nexity.dev.test.rollershutter.persistence.RollerShutterDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class RollerShutterController {

    @Autowired
    private RollerShutterDao rollerShutterDao;

    @RequestMapping("/rollershutter/{id}")
    public ResponseEntity<RollerShutter> get(@PathVariable("id") long id){
        return getAdaptedStatusResponse(rollerShutterDao.get(id), HttpStatus.OK, HttpStatus.NO_CONTENT);
    }

    @PostMapping("/rollershutter")
    public ResponseEntity<RollerShutter> create(@RequestBody RollerShutter rollerShutter){
        if(RollerShutterValidator.isValidForCreation(rollerShutter)){
            return new ResponseEntity<RollerShutter>(rollerShutterDao.create(rollerShutter), HttpStatus.CREATED);
        }
        else{
            return new ResponseEntity(RollerShutterValidator.IS_VALID_FOR_CREATION_ERROR, HttpStatus.BAD_REQUEST);
        }
    }

    @PatchMapping("/rollershutter/{id}")
    public ResponseEntity<RollerShutter> closing(@PathVariable("id") long id , @RequestParam("closing") int closingValue) {
        if(RollerShutterValidator.isClosingValueAcceptable(closingValue)){
            return getAdaptedStatusResponse(rollerShutterDao.updateClosing(id, closingValue), HttpStatus.OK, HttpStatus.NOT_FOUND);
        }
        else{
            return new ResponseEntity(RollerShutterValidator.IS_CLOSING_VALUE_ACCEPTABLE_ERROR, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @DeleteMapping("/rollershutter/{id}")
    public ResponseEntity<RollerShutter> delete(@PathVariable("id") long id){
        return getAdaptedStatusResponse(rollerShutterDao.delete(id), HttpStatus.OK, HttpStatus.NOT_FOUND);
    }



    /**
     * If the RollerShutter is null set errorStatus, else the awaitedStatus
     * @param rollerShutter
     * @param awaitedStatus
     * @param errorStatus
     * @return Response
     */
    private ResponseEntity<RollerShutter> getAdaptedStatusResponse(RollerShutter rollerShutter, HttpStatus awaitedStatus, HttpStatus errorStatus){
        if (rollerShutter == null){
            return new ResponseEntity(errorStatus);
        }
        return new ResponseEntity<RollerShutter>(rollerShutter, awaitedStatus);
    }
}
