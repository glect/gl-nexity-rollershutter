package com.nexity.dev.test.rollershutter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RollershutterApplication {

	public static void main(String[] args) {
		SpringApplication.run(RollershutterApplication.class, args);
	}

}

