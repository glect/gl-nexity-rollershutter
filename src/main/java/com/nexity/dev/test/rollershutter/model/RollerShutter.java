package com.nexity.dev.test.rollershutter.model;

/**
 * Define a RollerShutter  (Volet Roulant)
 */
public class RollerShutter {

    private long id;
    private String label;
    private String room;
    private int closing;
    private boolean isConnected;

    /**
     * Constructor
     * @param id
     * @param label
     * @param room
     * @param closing
     * @param isConnected
     */
    public RollerShutter(long id, String label, String room, int closing, boolean isConnected) {
        this.id = id;
        this.label = label;
        this.room = room;
        this.closing = closing;
        this.isConnected = isConnected;
    }
    public RollerShutter(){}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public int getClosing() {
        return closing;
    }

    public void setClosing(int closing) {
        this.closing = closing;
    }

    public boolean isConnected() {
        return isConnected;
    }

    public void setConnected(boolean connected) {
        isConnected = connected;
    }
}
