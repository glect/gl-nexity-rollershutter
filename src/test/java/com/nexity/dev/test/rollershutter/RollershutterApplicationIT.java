package com.nexity.dev.test.rollershutter;

import com.nexity.dev.test.rollershutter.model.RollerShutter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RollershutterApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RollershutterApplicationIT {

	@LocalServerPort
	private int localPort;
	private TestRestTemplate restTemplate = new TestRestTemplate();
	private	RestTemplate patchRestTemplate;
	private HttpHeaders headers = new HttpHeaders();

	@Before
	public void setup() {
		patchRestTemplate = restTemplate.getRestTemplate();
	}

	@Test
	public void should_retreive_rollerShutter() {
		ResponseEntity<RollerShutter> response = getRollerShutterResponseEntity(HttpMethod.GET, 1);
		RollerShutter rollerShutterRetreived = response.getBody();
		Assert.assertNotNull(rollerShutterRetreived);
		Assert.assertEquals(rollerShutterRetreived.getLabel(), "Volet exterieur veranda");
		Assert.assertEquals(rollerShutterRetreived.getRoom(), "Veranda");
		Assert.assertEquals(rollerShutterRetreived.getClosing(), 100);
		Assert.assertEquals(rollerShutterRetreived.isConnected(), true);
	}

	@Test
	public void should_create_rollerShutter(){
		RollerShutter newRollerShutter = new RollerShutter();
		newRollerShutter.setLabel("Volet de test");
		newRollerShutter.setRoom("Batcave");
		HttpEntity<RollerShutter> entity = new HttpEntity<RollerShutter>(newRollerShutter, headers);
		ResponseEntity<RollerShutter>  creationResponse =  restTemplate.exchange(
				getWsUrl("/rollershutter"),
				HttpMethod.POST, entity, RollerShutter.class);
		Assert.assertEquals(creationResponse.getStatusCode(), HttpStatus.CREATED);
		Assert.assertTrue(creationResponse.getBody().getId()>0);
	}

	@Test
	public void should_modify_closing(){
		int closing = 99;
		int idRollerShutter = 3;
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(getWsUrl("/rollershutter/"+ idRollerShutter)).queryParam("closing", closing);
		ResponseEntity<RollerShutter>  updatedClosingResponse = patchRestTemplate.exchange(
				builder.build().encode().toUri(),
				HttpMethod.PATCH,
				null,
				RollerShutter.class);
		Assert.assertEquals(updatedClosingResponse.getStatusCode(), HttpStatus.OK);
		Assert.assertEquals(updatedClosingResponse.getBody().getClosing(), closing);
	}

	@Test
	public void should_delete_rollerShutter() {
		ResponseEntity<RollerShutter> deleteResponse = getRollerShutterResponseEntity(HttpMethod.DELETE, 2);
		Assert.assertEquals(deleteResponse.getStatusCode(), HttpStatus.OK);
		ResponseEntity<RollerShutter> getDeletedObjectResponse = getRollerShutterResponseEntity(HttpMethod.GET, 2);
		Assert.assertEquals(getDeletedObjectResponse.getStatusCode(), HttpStatus.NO_CONTENT);
	}

	private ResponseEntity<RollerShutter> getRollerShutterResponseEntity(HttpMethod get, int id) {
		return restTemplate.exchange(
				getWsUrl("/rollershutter/" + id),
				get, null, RollerShutter.class);
	}

	private String getWsUrl(String path) {
		return ("http://localhost:" + localPort + path);
	}

}

